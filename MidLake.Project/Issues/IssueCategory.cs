﻿namespace MidLake.Project
{
    public enum IssueCategory
    {
        Feature,
        Bug,
        Improvement,
        Task
    }
}