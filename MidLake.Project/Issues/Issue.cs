﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidLake.Project.Issues
{
    public class Issue
    {
        public Issue(int number, string name)
        {
            Name = name;
            Number = number;
        }

        public string Name;
        public string Summary;
        public int Number;
        public IssueStatus Status;
        public IssuePriority Priority;
        public IssueCategory Category;
    }
}
