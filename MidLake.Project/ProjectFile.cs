﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidLake.Project.Issues;

namespace MidLake.Project
{
    public class ProjectFile
    {
        public ProjectFile(string name, string id)
        {
            Name = name;
            Identifier = id;
        }

        public string Name;
        public string Identifier;
        public int LastIssue => Issues.Count;
        public List<Issue> Issues { get; set; } = new List<Issue>();
    }
}
