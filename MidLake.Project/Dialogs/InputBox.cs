﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidLake.Project.Dialogs
{
    public partial class InputBox : Form
    {
        public InputBox(string title, string description, Form owner)
        {
            InitializeComponent();
            this.Owner = owner;
            Text = title;
            labelDescription.Text = description;
        }

        public string ResultText { get; private set; }

        private void InputBox_Load(object sender, EventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            ResultText = textBox1.Text;
            Debug.WriteLine(ResultText);
            Close();
        }
    }
}
