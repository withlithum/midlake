﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Project.Issues;
using Newtonsoft.Json;

namespace MidLake.Project.Util
{
    public static class ProjectManager
    {
        public const int ProjectHeaderVersion = 1;
        public static readonly string DataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "MidLake Data");
        public static Dictionary<string, string> IDToNameMap = new Dictionary<string, string>();
        public static readonly string IndexPath = Path.Combine(DataFolder, "projectIndex.json");

        public static void CheckAndFixDataFolder()
        {
            Directory.CreateDirectory(DataFolder);
            Directory.CreateDirectory(Path.Combine(DataFolder, "Projects"));
            string json = JsonConvert.SerializeObject(IDToNameMap);
            File.WriteAllText(IndexPath, json);
        }

        public static void DeleteProject(string id)
        {
            CheckAndFixDataFolder();
            string projectFolder = Path.Combine(DataFolder, "Projects\\" + id + ".phf");
            if (File.Exists(projectFolder))
            {
                File.Delete(projectFolder);
                IDToNameMap.Remove(id);
            }
            CheckAndFixDataFolder();
        }

        public static void Init()
        {
            if (File.Exists(IndexPath))
            {
                string json = File.ReadAllText(Path.Combine(DataFolder, "projectIndex.json"));
                IDToNameMap = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }
        }

        static ProjectManager()
        {
            
        }

        public static void SaveProjectHeaderFile(ProjectFile project)
        {
            CheckAndFixDataFolder();
            string projectFolder = Path.Combine(DataFolder, "Projects\\" + project.Identifier + ".phf");
            string str = JsonConvert.SerializeObject(project);
            File.WriteAllText(projectFolder, str);
            if (!IDToNameMap.ContainsKey(project.Identifier))
            {
                IDToNameMap.Add(project.Identifier, project.Name);
            }
        }

        public static ProjectFile LoadProjectHeaderFile(string id)
        {
            CheckAndFixDataFolder();
            string projectFolder = Path.Combine(DataFolder, "Projects\\" + id + ".phf");
            Debug.WriteLine(projectFolder);
            if (!File.Exists(projectFolder))
            {
                MessageBox.Show(Strings.MessageInvalidProject, Strings.MessageError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            ProjectFile result = JsonConvert.DeserializeObject<ProjectFile>(File.ReadAllText(projectFolder));
            return result;
        }
    }
}
