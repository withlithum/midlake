﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Framework.Util;
using MidLake.Project.Util;
using MidLake.Util;
using NLog;

namespace MidLake
{
    public partial class MainFrm : Form
    {
        private ItemFrm manager;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private ImmersiveInfo info = ImmersiveCasts.GetNewOne();

        public MainFrm()
        {
            InitializeComponent();
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            ProjectManager.Init();
            logger.Info("Initialized project manager");
            this.BackColor = info.Background;
            this.ForeColor = info.Foreground;
            menuMain.BackColor = info.Background;
            menuMain.ForeColor = info.Foreground;
            itemProject.BackColor = info.Background;
            itemProject.ForeColor = info.Foreground;
        }

        private void itemNewProject_Click(object sender, EventArgs e)
        {
            
            logger.Info("Creating a new project");
            NewProject np = new NewProject();
            var dr = np.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (manager != null && !manager.IsDisposed && !manager.Disposing)
                {
                    logger.Info("Disposing manager");
                    if (manager.Visible)
                    {
                        manager.Close();
                    }

                    manager.Dispose();
                    manager = null;
                }
                ProjectManager.SaveProjectHeaderFile(np.Infos);
                manager = new ItemFrm(this);
                manager.Show();
                logger.Info($"Created project: {np.Infos.Identifier} ({np.Infos.Name})");
                manager.OpenProject(np.Infos.Identifier);
            }
        }

        private void buttonOpenProject_Click(object sender, EventArgs e)
        {
            logger.Info("Promoted to open an existing project");
            OpenProjectDialog opd = new OpenProjectDialog();
            DialogResult dr = opd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                logger.Info("Attempting to dispose manager");
                if (manager != null && !manager.IsDisposed && !manager.Disposing)
                {
                    if (manager.Visible)
                    {
                        manager.Close();
                    }

                    manager.Dispose();
                    manager = null;
                }
                manager = new ItemFrm(this);
                manager.Show();
                manager.OpenProject(opd.SelectedProject);
                logger.Info("Successfully loaded a new manager with project " + opd.SelectedProject);
            }
        }

        private void itemAbout_Click(object sender, EventArgs e)
        {
            new About().Show();
        }

        private void itemCreateIssue_Click(object sender, EventArgs e)
        {
            if (manager == null)
            {
                MessageBox.Show("尚未打开项目。请建立一个新的项目，或打开一个现有项目。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            manager.CreateIssue();
        }

        private void itemReopenManager_Click(object sender, EventArgs e)
        {
            if (manager == null)
            {
                MessageBox.Show("尚未打开项目。请建立一个新的项目，或打开一个现有项目。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string projectId = manager.project.Identifier;
            if (manager != null && !manager.IsDisposed && !manager.Disposing)
            {
                if (manager.Visible)
                {
                    manager.Close();
                }

                manager.Dispose();
                manager = null;
            }
            
            manager = new ItemFrm(this);
            manager.Show();
            manager.OpenProject(projectId);
        }

        private void itemViewLog_Click(object sender, EventArgs e)
        {
            Process.Start("MidLake.log");
        }

        private void itemSave_Click(object sender, EventArgs e)
        {
            if (manager == null)
            {
                MessageBox.Show("尚未打开项目。请建立一个新的项目，或打开一个现有项目。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            manager.Save();
        }
    }
}
