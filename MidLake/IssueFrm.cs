﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Project;
using MidLake.Project.Issues;

namespace MidLake
{
    public partial class IssueFrm : Form
    {
        ProjectFile pf;
        ItemFrm managerFrm;
        int issueNumber;

        bool bold;
        bool italic;

        public IssueFrm(ProjectFile project, ItemFrm manager)
        {
            InitializeComponent();
            pf = project;
            issueNumber = pf.LastIssue + 1;
            managerFrm = manager;
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 2;
        }

        public IssueFrm(ProjectFile project, ItemFrm manager, int number) : this(project, manager)
        {
            issueNumber = number;
            Issue issue = project.Issues[number - 1];
            textName.Text = issue.Name;
            textSummary.Rtf = issue.Summary;
            comboBox1.SelectedIndex = (int)issue.Status;
            comboBox2.SelectedIndex = (int)issue.Priority;
            comboCategory.SelectedIndex = (int)issue.Category;
        }

        private void IssueFrm_Load(object sender, EventArgs e)
        {
            labelNumber.Text = "问题编号 #" + issueNumber;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textName.Text))
            {
                MessageBox.Show("问题名称不能为空。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (comboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择一个状态。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Issue i = new Issue(this.issueNumber, textName.Text);
            i.Summary = textSummary.Rtf;
            i.Status = (IssueStatus)comboBox1.SelectedIndex;
            i.Priority = (IssuePriority)comboBox2.SelectedIndex;
            i.Category = (IssueCategory)comboCategory.SelectedIndex;
            if ((pf.Issues.Count - 1) >= (issueNumber - 1))
            {
                pf.Issues[issueNumber - 1] = i;
            }
            else
            {
                pf.Issues.Add(i);
            }
            managerFrm.UpdateList();
            managerFrm.SetAsChanged();
            Close();
        }

        private void IssueFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            managerFrm.SetBackAddIssue();
        }

        private void buttonBold_Click(object sender, EventArgs e)
        {
            
        }

        private void SetSelectionFont()
        {
            FontStyle s = FontStyle.Regular;

            if (italic)
            {
                s = FontStyle.Italic;
            }
            if (bold)
            {
                s = FontStyle.Bold;
                if (italic)
                {
                    s = FontStyle.Bold | FontStyle.Italic;
                }
            }
            textSummary.SelectionFont = new Font(textSummary.SelectionFont, s);
        }

        private void checkBold_CheckedChanged(object sender, EventArgs e)
        {
            bold = checkBold.Checked;

            SetSelectionFont();
        }

        private void checkItalic_CheckedChanged(object sender, EventArgs e)
        {
            italic = checkItalic.Checked;
            SetSelectionFont();
        }

        private void textSummary_SelectionChanged(object sender, EventArgs e)
        {
            if (textSummary.SelectionFont.Bold)
            {
                bold = true;
                checkBold.Checked = true;
            }
            if (textSummary.SelectionFont.Italic)
            {
                italic = true;
                checkItalic.Checked = true;
            }
            if (textSummary.SelectionFont.Style == FontStyle.Regular)
            {
                bold = false;
                italic = false;
                checkBold.Checked = false;
                checkItalic.Checked = false;
            }
        }
    }
}
