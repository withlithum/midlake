﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Framework.Util;
using MidLake.Project;
using MidLake.Project.Dialogs;
using MidLake.Project.Issues;
using MidLake.Project.Util;
using MidLake.Util;
using NLog;

namespace MidLake
{
    public partial class ItemFrm : Form
    {
        private bool saved = false;
        private bool changed = false;
        private DateTime lastSave = DateTime.Now;
        MainFrm par;
        internal ProjectFile project;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public ItemFrm(MainFrm parent)
        {
            InitializeComponent();
            this.MdiParent = parent;
            par = parent;
            logger.Info("Created a new manager");
        }

        internal void SetBackAddIssue()
        {
            this.buttonAddIssue.Enabled = true;
        }

        private void ItemFrm_Load(object sender, EventArgs e)
        {

        }

        internal void OpenProject()
        {
            if (!File.Exists(ProjectManager.IndexPath))
            {
                InputBox ib = new InputBox("打开现有项目", "无法找到索引文件。请输入一个 ID，用于打开手动放置的文件。", this);
                if (ib.ShowDialog() == DialogResult.OK)
                {
                    ProjectFile pf = ProjectManager.LoadProjectHeaderFile(ib.ResultText);
                    if (pf == null)
                    {
                        return;
                    }
                    project = pf;
                    saved = true;
                    buttonAddIssue.Enabled = true;
                    UpdateContext();
                    changed = false;
                }
            }
            else
            {
                OpenProjectDialog opd = new OpenProjectDialog();
                opd.Owner = this;
                if (opd.ShowDialog() == DialogResult.OK)
                {
                    ProjectFile pro = ProjectManager.LoadProjectHeaderFile(opd.SelectedProject);
                    if (pro == null)
                    {
                        return;
                    }
                    project = pro;
                    saved = true;
                    buttonAddIssue.Enabled = true;
                    
                    UpdateContext();
                    changed = false;
                }
            }
        }

        internal void OpenProject(string identifier)
        {
            logger.Info("Opening project " + identifier);
            ProjectFile pf = ProjectManager.LoadProjectHeaderFile(identifier);
            if (pf == null)
            {
                logger.Warn("The project file is null - aborting project opening");
                return;
            }
            project = pf;
            saved = true;
            buttonAddIssue.Enabled = true;
            UpdateContext();
            changed = false;
        }

        internal void SetAsChanged()
        {
            if (!changed)
            {
                this.Text = "*" + this.Text;
            }
            
            changed = true;
        }

        private void buttonOpenExisting_Click(object sender, EventArgs e)
        {
            OpenProject();
            
        }

        internal void UpdateContext()
        {
            this.labelName.Text = project.Name;
            UpdateList();
        }

        internal void UpdateList()
        {
            logger.Info("Updating issues list");
            listIssues.Items.Clear();
            foreach (var issue in project.Issues)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = issue.Number.ToString();
                lvi.SubItems.Add(issue.Category.ToString());
                lvi.SubItems.Add(issue.Name);
                lvi.SubItems.Add(issue.Status.ToString());
                lvi.SubItems.Add(issue.Priority.ToString());
                if (issue.Status == IssueStatus.Done)
                {
                    lvi.BackColor = Color.Gray;
                    lvi.Font = new Font(this.Font, FontStyle.Strikeout | FontStyle.Italic);
                }
                if (issue.Priority == IssuePriority.High || issue.Priority == IssuePriority.VeryHigh)
                {
                    lvi.BackColor = Color.Red;
                    lvi.ForeColor = Color.White;
                }
                listIssues.Items.Add(lvi);
            }
        }

        internal bool Save()
        {
            //if (string.IsNullOrWhiteSpace(textName.Text) || string.IsNullOrWhiteSpace(textId.Text))
            //{
            //    MessageBox.Show("项目名称或 ID 不能为空。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return false;
            //}

            //if (project == null)
            //{
            //    project = new ProjectFile(textName.Text, textId.Text);
            //}

            ProjectManager.SaveProjectHeaderFile(project);
            if (changed)
            {
                this.Text = "项目管理器";
            }
            saved = true;
            changed = false;
            lastSave = DateTime.Now;
            buttonAddIssue.Enabled = true;
            
            return true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        internal void CreateIssue()
        {
            logger.Info("Opening issue creating dialog");
            if (project == null)
            {
                MessageBox.Show("在保存项目之前，不能创建问题。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            IssueFrm issueFrm = new IssueFrm(project, this);
            issueFrm.MdiParent = par;
            this.buttonAddIssue.Enabled = false;
            issueFrm.Show();
        }

        private void buttonAddIssue_Click(object sender, EventArgs e)
        {
            CreateIssue();
        }

        private void listIssues_DoubleClick(object sender, EventArgs e)
        {
            if (listIssues.Items.Count > 0 && listIssues.SelectedIndices[0] != -1)
            {
                IssueFrm issueFrm = new IssueFrm(project, this, listIssues.SelectedIndices[0] + 1);
                issueFrm.MdiParent = par;
                issueFrm.Show();
            }
        }

        private string GetFormattedStringLength(TimeSpan ts)
        {
            bool hour = ts.Hours != 0;
            bool minute = ts.Minutes != 0;
            

            string format = "%s' 秒'";

            if (minute)
            {
                format = "%m' 分钟 '" + format;
            }
            if (hour)
            {
                format = "%h' 小时 '" + format;
            }

            Debug.WriteLine(format);
            return ts.ToString(format);
        }

        private void ItemFrm_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (changed)
            {
                TimeSpan elimatedTimed = lastSave - DateTime.Now;

                DialogResult dr = MessageBox.Show($"是否保存？如不保存，你在 {GetFormattedStringLength(elimatedTimed)} 内的更改将全部丢失。", "关闭", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    bool success = Save();
                    if (!success)
                    {
                        MessageBox.Show("因未能保存，关闭操作已取消。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
                if (dr == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
            
        }

        private void textName_TextChanged(object sender, EventArgs e)
        {
            changed = true;
        }

        private void textId_TextChanged(object sender, EventArgs e)
        {
            changed = true;
        }

        private void ItemFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void listIssues_ItemActivate(object sender, EventArgs e)
        {
            
        }

        private void itemDelete_Click(object sender, EventArgs e)
        {
            if (listIssues.SelectedIndices.Count == 0)
            {
                return;
            }

            int toDelete = listIssues.SelectedIndices[0];

            List<Issue> temp = project.Issues;
            ListHelper.DeleteIssueFromList(ref temp, project.Issues[toDelete]);
            project.Issues = temp;
            UpdateList();
        }

        private void menuIssue_Opening(object sender, CancelEventArgs e)
        {
            itemDelete.Enabled = listIssues.Items.Count != 0 && listIssues.SelectedIndices.Count != 0;
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {

        }
    }
}
