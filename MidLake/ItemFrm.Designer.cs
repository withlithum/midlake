﻿
namespace MidLake
{
    partial class ItemFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemFrm));
            this.labelName = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAddIssue = new System.Windows.Forms.Button();
            this.buttonOpenExisting = new System.Windows.Forms.Button();
            this.listIssues = new System.Windows.Forms.ListView();
            this.headerId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.headerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.headerPriority = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.headerStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuIssue = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.itemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCreateNew = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.headerCategory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuIssue.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelName.Location = new System.Drawing.Point(16, 16);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(110, 31);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "未知项目";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(716, 509);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(149, 35);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "保存文档";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAddIssue
            // 
            this.buttonAddIssue.Enabled = false;
            this.buttonAddIssue.Location = new System.Drawing.Point(568, 510);
            this.buttonAddIssue.Name = "buttonAddIssue";
            this.buttonAddIssue.Size = new System.Drawing.Size(141, 35);
            this.buttonAddIssue.TabIndex = 5;
            this.buttonAddIssue.Text = "添加问题";
            this.buttonAddIssue.UseVisualStyleBackColor = true;
            this.buttonAddIssue.Click += new System.EventHandler(this.buttonAddIssue_Click);
            // 
            // buttonOpenExisting
            // 
            this.buttonOpenExisting.Location = new System.Drawing.Point(20, 509);
            this.buttonOpenExisting.Name = "buttonOpenExisting";
            this.buttonOpenExisting.Size = new System.Drawing.Size(145, 35);
            this.buttonOpenExisting.TabIndex = 7;
            this.buttonOpenExisting.Text = "打开项目...";
            this.buttonOpenExisting.UseVisualStyleBackColor = true;
            this.buttonOpenExisting.Click += new System.EventHandler(this.buttonOpenExisting_Click);
            // 
            // listIssues
            // 
            this.listIssues.AllowColumnReorder = true;
            this.listIssues.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.headerId,
            this.headerCategory,
            this.headerName,
            this.headerPriority,
            this.headerStatus});
            this.listIssues.ContextMenuStrip = this.menuIssue;
            this.listIssues.FullRowSelect = true;
            this.listIssues.HideSelection = false;
            this.listIssues.Location = new System.Drawing.Point(22, 50);
            this.listIssues.MultiSelect = false;
            this.listIssues.Name = "listIssues";
            this.listIssues.Size = new System.Drawing.Size(843, 453);
            this.listIssues.TabIndex = 8;
            this.listIssues.UseCompatibleStateImageBehavior = false;
            this.listIssues.View = System.Windows.Forms.View.Details;
            this.listIssues.ItemActivate += new System.EventHandler(this.listIssues_ItemActivate);
            this.listIssues.DoubleClick += new System.EventHandler(this.listIssues_DoubleClick);
            // 
            // headerId
            // 
            this.headerId.Text = "ID";
            // 
            // headerName
            // 
            this.headerName.DisplayIndex = 1;
            this.headerName.Text = "名称";
            this.headerName.Width = 354;
            // 
            // headerPriority
            // 
            this.headerPriority.DisplayIndex = 2;
            this.headerPriority.Text = "优先级";
            this.headerPriority.Width = 94;
            // 
            // headerStatus
            // 
            this.headerStatus.DisplayIndex = 3;
            this.headerStatus.Text = "状态";
            // 
            // menuIssue
            // 
            this.menuIssue.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuIssue.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemDelete,
            this.itemCreateNew});
            this.menuIssue.Name = "menuIssue";
            this.menuIssue.Size = new System.Drawing.Size(198, 64);
            this.menuIssue.Text = "Issues";
            this.menuIssue.Opening += new System.ComponentModel.CancelEventHandler(this.menuIssue_Opening);
            // 
            // itemDelete
            // 
            this.itemDelete.Name = "itemDelete";
            this.itemDelete.Size = new System.Drawing.Size(197, 30);
            this.itemDelete.Text = "删除问题(&D)";
            this.itemDelete.Click += new System.EventHandler(this.itemDelete_Click);
            // 
            // itemCreateNew
            // 
            this.itemCreateNew.Name = "itemCreateNew";
            this.itemCreateNew.Size = new System.Drawing.Size(197, 30);
            this.itemCreateNew.Text = "创建新问题(&N)";
            // 
            // buttonSettings
            // 
            this.buttonSettings.Location = new System.Drawing.Point(421, 509);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(141, 36);
            this.buttonSettings.TabIndex = 9;
            this.buttonSettings.Text = "项目设置";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // headerCategory
            // 
            this.headerCategory.DisplayIndex = 4;
            this.headerCategory.Text = "类别";
            // 
            // ItemFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 557);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.listIssues);
            this.Controls.Add(this.buttonOpenExisting);
            this.Controls.Add(this.buttonAddIssue);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.labelName);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemFrm";
            this.Text = "项目管理器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItemFrm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ItemFrm_FormClosed);
            this.Load += new System.EventHandler(this.ItemFrm_Load);
            this.menuIssue.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAddIssue;
        private System.Windows.Forms.Button buttonOpenExisting;
        private System.Windows.Forms.ListView listIssues;
        private System.Windows.Forms.ColumnHeader headerId;
        private System.Windows.Forms.ColumnHeader headerName;
        private System.Windows.Forms.ColumnHeader headerPriority;
        private System.Windows.Forms.ColumnHeader headerStatus;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.ContextMenuStrip menuIssue;
        private System.Windows.Forms.ToolStripMenuItem itemDelete;
        private System.Windows.Forms.ToolStripMenuItem itemCreateNew;
        private System.Windows.Forms.ColumnHeader headerCategory;
    }
}