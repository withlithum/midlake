﻿
namespace MidLake
{
    partial class IssueFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueFrm));
            this.labelNumber = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.labelSummary = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.Priority = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textSummary = new System.Windows.Forms.RichTextBox();
            this.checkBold = new System.Windows.Forms.CheckBox();
            this.checkItalic = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboCategory = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelNumber.Location = new System.Drawing.Point(12, 9);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(146, 31);
            this.labelNumber.TabIndex = 0;
            this.labelNumber.Text = "问题编号 #0";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(14, 49);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(46, 24);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "名称";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(66, 46);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(620, 30);
            this.textName.TabIndex = 2;
            // 
            // labelSummary
            // 
            this.labelSummary.AutoSize = true;
            this.labelSummary.Location = new System.Drawing.Point(14, 88);
            this.labelSummary.Name = "labelSummary";
            this.labelSummary.Size = new System.Drawing.Size(46, 24);
            this.labelSummary.TabIndex = 3;
            this.labelSummary.Text = "简述";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(717, 49);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(46, 24);
            this.labelStatus.TabIndex = 5;
            this.labelStatus.Text = "状态";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "开放",
            "进行中",
            "关闭",
            "完成"});
            this.comboBox1.Location = new System.Drawing.Point(769, 46);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(177, 32);
            this.comboBox1.TabIndex = 6;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(833, 285);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(113, 35);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "保存";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // Priority
            // 
            this.Priority.AutoSize = true;
            this.Priority.Location = new System.Drawing.Point(699, 87);
            this.Priority.Name = "Priority";
            this.Priority.Size = new System.Drawing.Size(64, 24);
            this.Priority.TabIndex = 8;
            this.Priority.Text = "优先级";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "最高",
            "高",
            "中",
            "低",
            "最低"});
            this.comboBox2.Location = new System.Drawing.Point(769, 84);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(177, 32);
            this.comboBox2.TabIndex = 9;
            // 
            // textSummary
            // 
            this.textSummary.Location = new System.Drawing.Point(18, 146);
            this.textSummary.Name = "textSummary";
            this.textSummary.Size = new System.Drawing.Size(668, 174);
            this.textSummary.TabIndex = 10;
            this.textSummary.Text = "";
            this.textSummary.SelectionChanged += new System.EventHandler(this.textSummary_SelectionChanged);
            // 
            // checkBold
            // 
            this.checkBold.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBold.Image = ((System.Drawing.Image)(resources.GetObject("checkBold.Image")));
            this.checkBold.Location = new System.Drawing.Point(18, 115);
            this.checkBold.Name = "checkBold";
            this.checkBold.Size = new System.Drawing.Size(28, 25);
            this.checkBold.TabIndex = 11;
            this.checkBold.UseVisualStyleBackColor = true;
            this.checkBold.CheckedChanged += new System.EventHandler(this.checkBold_CheckedChanged);
            // 
            // checkItalic
            // 
            this.checkItalic.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkItalic.Image = ((System.Drawing.Image)(resources.GetObject("checkItalic.Image")));
            this.checkItalic.Location = new System.Drawing.Point(52, 115);
            this.checkItalic.Name = "checkItalic";
            this.checkItalic.Size = new System.Drawing.Size(28, 25);
            this.checkItalic.TabIndex = 12;
            this.checkItalic.UseVisualStyleBackColor = true;
            this.checkItalic.CheckedChanged += new System.EventHandler(this.checkItalic_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(717, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 24);
            this.label1.TabIndex = 13;
            this.label1.Text = "类别";
            // 
            // comboCategory
            // 
            this.comboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCategory.FormattingEnabled = true;
            this.comboCategory.Items.AddRange(new object[] {
            "功能请求",
            "故障",
            "改进",
            "任务"});
            this.comboCategory.Location = new System.Drawing.Point(769, 122);
            this.comboCategory.Name = "comboCategory";
            this.comboCategory.Size = new System.Drawing.Size(177, 32);
            this.comboCategory.TabIndex = 14;
            // 
            // IssueFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 333);
            this.Controls.Add(this.comboCategory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkItalic);
            this.Controls.Add(this.checkBold);
            this.Controls.Add(this.textSummary);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.Priority);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelSummary);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelNumber);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IssueFrm";
            this.Text = "问题管理器";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IssueFrm_FormClosed);
            this.Load += new System.EventHandler(this.IssueFrm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label labelSummary;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label Priority;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.RichTextBox textSummary;
        private System.Windows.Forms.CheckBox checkBold;
        private System.Windows.Forms.CheckBox checkItalic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboCategory;
    }
}