﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Project;

namespace MidLake.Util
{
    public partial class NewProject : Form
    {
        public NewProject()
        {
            InitializeComponent();
        }

        public ProjectFile Infos;

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textName.Text) || string.IsNullOrWhiteSpace(textId.Text))
            {
                MessageBox.Show("请先填写名称和标识符。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Infos = new ProjectFile(textName.Text, textId.Text);
            Close();
        }
    }
}
