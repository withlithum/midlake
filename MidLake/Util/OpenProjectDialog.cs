﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Project.Util;

namespace MidLake.Util
{
    public partial class OpenProjectDialog : Form
    {
        private Dictionary<int, string> IndexToIDMap = new Dictionary<int, string>();
        internal string SelectedProject;

        public OpenProjectDialog()
        {
            InitializeComponent();
        }

        private void OpenProjectDialog_Load(object sender, EventArgs e)
        {
            foreach (var id in ProjectManager.IDToNameMap.Keys)
            {
                int index = listProjects.Items.Add($"{ProjectManager.IDToNameMap[id]} ({id})");
                IndexToIDMap.Add(index, id);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            SelectedProject = IndexToIDMap[listProjects.SelectedIndex];
            Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("删除项目将一并删除其所有的问题信息，并且无法找回。确认要删除吗？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                string id = IndexToIDMap[listProjects.SelectedIndex];
                ProjectManager.DeleteProject(id);
                listProjects.Items.Remove(listProjects.SelectedItem);
            }
            
        }

        private void buttonCreateNew_Click(object sender, EventArgs e)
        {
            NewProject np = new NewProject();
            DialogResult dr = np.ShowDialog();
            if (dr == DialogResult.OK)
            {
                ProjectManager.SaveProjectHeaderFile(np.Infos);
                SelectedProject = np.Infos.Identifier;
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
