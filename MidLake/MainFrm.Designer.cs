﻿
namespace MidLake
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.itemProject = new System.Windows.Forms.ToolStripMenuItem();
            this.itemNewProject = new System.Windows.Forms.ToolStripMenuItem();
            this.itemOpenExisting = new System.Windows.Forms.ToolStripMenuItem();
            this.seperator = new System.Windows.Forms.ToolStripSeparator();
            this.itemCreateIssue = new System.Windows.Forms.ToolStripMenuItem();
            this.itemReopenManager = new System.Windows.Forms.ToolStripMenuItem();
            this.itemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.itemViewLog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.buttonNewProject = new System.Windows.Forms.ToolStripButton();
            this.buttonOpenProject = new System.Windows.Forms.ToolStripButton();
            this.itemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMain.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemProject,
            this.itemHelp});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(800, 32);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            // 
            // itemProject
            // 
            this.itemProject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemNewProject,
            this.itemOpenExisting,
            this.itemSave,
            this.seperator,
            this.itemCreateIssue,
            this.itemReopenManager});
            this.itemProject.Name = "itemProject";
            this.itemProject.Size = new System.Drawing.Size(85, 28);
            this.itemProject.Text = "工程(&P)";
            // 
            // itemNewProject
            // 
            this.itemNewProject.Image = ((System.Drawing.Image)(resources.GetObject("itemNewProject.Image")));
            this.itemNewProject.Name = "itemNewProject";
            this.itemNewProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.itemNewProject.Size = new System.Drawing.Size(314, 34);
            this.itemNewProject.Text = "建立新项目(&N)";
            this.itemNewProject.Click += new System.EventHandler(this.itemNewProject_Click);
            // 
            // itemOpenExisting
            // 
            this.itemOpenExisting.Image = ((System.Drawing.Image)(resources.GetObject("itemOpenExisting.Image")));
            this.itemOpenExisting.Name = "itemOpenExisting";
            this.itemOpenExisting.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.itemOpenExisting.Size = new System.Drawing.Size(314, 34);
            this.itemOpenExisting.Text = "打开现有项目(&O)";
            this.itemOpenExisting.Click += new System.EventHandler(this.buttonOpenProject_Click);
            // 
            // seperator
            // 
            this.seperator.Name = "seperator";
            this.seperator.Size = new System.Drawing.Size(311, 6);
            // 
            // itemCreateIssue
            // 
            this.itemCreateIssue.Name = "itemCreateIssue";
            this.itemCreateIssue.Size = new System.Drawing.Size(314, 34);
            this.itemCreateIssue.Text = "创建问题(&C)";
            this.itemCreateIssue.Click += new System.EventHandler(this.itemCreateIssue_Click);
            // 
            // itemReopenManager
            // 
            this.itemReopenManager.Name = "itemReopenManager";
            this.itemReopenManager.Size = new System.Drawing.Size(314, 34);
            this.itemReopenManager.Text = "重新打开项目管理器(&R)";
            this.itemReopenManager.Click += new System.EventHandler(this.itemReopenManager_Click);
            // 
            // itemHelp
            // 
            this.itemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemAbout,
            this.toolStripMenuItem1,
            this.itemViewLog});
            this.itemHelp.Name = "itemHelp";
            this.itemHelp.Size = new System.Drawing.Size(88, 28);
            this.itemHelp.Text = "帮助(&H)";
            // 
            // itemAbout
            // 
            this.itemAbout.Name = "itemAbout";
            this.itemAbout.Size = new System.Drawing.Size(270, 34);
            this.itemAbout.Text = "关于(&B)";
            this.itemAbout.Click += new System.EventHandler(this.itemAbout_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(267, 6);
            // 
            // itemViewLog
            // 
            this.itemViewLog.Name = "itemViewLog";
            this.itemViewLog.Size = new System.Drawing.Size(270, 34);
            this.itemViewLog.Text = "查看日志(&L)";
            this.itemViewLog.Click += new System.EventHandler(this.itemViewLog_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonNewProject,
            this.buttonOpenProject});
            this.toolStrip.Location = new System.Drawing.Point(0, 32);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(800, 33);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // buttonNewProject
            // 
            this.buttonNewProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonNewProject.Image = ((System.Drawing.Image)(resources.GetObject("buttonNewProject.Image")));
            this.buttonNewProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonNewProject.Name = "buttonNewProject";
            this.buttonNewProject.Size = new System.Drawing.Size(34, 28);
            this.buttonNewProject.Text = "新建工程";
            this.buttonNewProject.Click += new System.EventHandler(this.itemNewProject_Click);
            // 
            // buttonOpenProject
            // 
            this.buttonOpenProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonOpenProject.Image = ((System.Drawing.Image)(resources.GetObject("buttonOpenProject.Image")));
            this.buttonOpenProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonOpenProject.Name = "buttonOpenProject";
            this.buttonOpenProject.Size = new System.Drawing.Size(34, 28);
            this.buttonOpenProject.Text = "打开工程";
            this.buttonOpenProject.Click += new System.EventHandler(this.buttonOpenProject_Click);
            // 
            // itemSave
            // 
            this.itemSave.Image = ((System.Drawing.Image)(resources.GetObject("itemSave.Image")));
            this.itemSave.Name = "itemSave";
            this.itemSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.itemSave.Size = new System.Drawing.Size(314, 34);
            this.itemSave.Text = "保存项目文件(&S)";
            this.itemSave.Click += new System.EventHandler(this.itemSave_Click);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuMain;
            this.Name = "MainFrm";
            this.Text = "MidLake";
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripMenuItem itemProject;
        private System.Windows.Forms.ToolStripMenuItem itemHelp;
        private System.Windows.Forms.ToolStripMenuItem itemAbout;
        private System.Windows.Forms.ToolStripMenuItem itemNewProject;
        private System.Windows.Forms.ToolStripButton buttonNewProject;
        private System.Windows.Forms.ToolStripButton buttonOpenProject;
        private System.Windows.Forms.ToolStripMenuItem itemOpenExisting;
        private System.Windows.Forms.ToolStripSeparator seperator;
        private System.Windows.Forms.ToolStripMenuItem itemCreateIssue;
        private System.Windows.Forms.ToolStripMenuItem itemReopenManager;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem itemViewLog;
        private System.Windows.Forms.ToolStripMenuItem itemSave;
    }
}

