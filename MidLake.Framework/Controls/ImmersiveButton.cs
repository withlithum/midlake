﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidLake.Framework.Util;

namespace MidLake.Framework.Controls
{
    public partial class ImmersiveButton : UserControl, IThemeable
    {
        private bool border;
        private Pen borderPen;
        private Rectangle rectangle;

        public ImmersiveButton() : this(ImmersiveCasts.GetNewOne())
        { 
        }

        /// <summary>
        /// 初始化 <see cref="ImmersiveButton"/> 类的新实例。
        /// </summary>
        public ImmersiveButton(ImmersiveInfo immer)
        {
            InitializeComponent();
            this.BackColor = immer.Background;
            this.ForeColor = immer.Foreground;
            ImmersiveInfo = immer;

            borderPen = new Pen(ImmersiveInfo.Foreground);
            rectangle = new Rectangle(0, 0, Width, Height);
        }

        public ImmersiveInfo ImmersiveInfo { get; set; }

        private void labelContext_Click(object sender, EventArgs e)
        {
            OnClick(e);
        }

        private void labelContext_MouseEnter(object sender, EventArgs e)
        {
            border = true;
        }

        private void labelContext_MouseLeave(object sender, EventArgs e)
        {
            border = false;
        }

        private void labelContext_Paint(object sender, PaintEventArgs e)
        {
            if (border)
            {
                e.Graphics.DrawRectangle(borderPen, rectangle);
            }
        }
    }
}
