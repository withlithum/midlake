﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidLake.Framework.Util;

namespace MidLake.Framework.Controls
{
    /// <summary>
    /// 表示一个能够被使用主题的控件。
    /// </summary>
    public interface IThemeable
    {
        /// <summary>
        /// 获取或设置此控件的 <see cref="Util.ImmersiveInfo"/>.
        /// </summary>
        ImmersiveInfo ImmersiveInfo { get; set; }
    }
}
