﻿
namespace MidLake.Framework.Controls
{
    partial class ImmersiveButton
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.labelContext = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelContext
            // 
            this.labelContext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelContext.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelContext.Location = new System.Drawing.Point(0, 0);
            this.labelContext.Name = "labelContext";
            this.labelContext.Size = new System.Drawing.Size(173, 42);
            this.labelContext.TabIndex = 0;
            this.labelContext.Text = "Immersive";
            this.labelContext.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelContext.Click += new System.EventHandler(this.labelContext_Click);
            this.labelContext.Paint += new System.Windows.Forms.PaintEventHandler(this.labelContext_Paint);
            this.labelContext.MouseEnter += new System.EventHandler(this.labelContext_MouseEnter);
            this.labelContext.MouseLeave += new System.EventHandler(this.labelContext_MouseLeave);
            // 
            // ImmersiveButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelContext);
            this.Name = "ImmersiveButton";
            this.Size = new System.Drawing.Size(173, 42);
            this.MouseEnter += new System.EventHandler(this.labelContext_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.labelContext_MouseLeave);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelContext;
    }
}
