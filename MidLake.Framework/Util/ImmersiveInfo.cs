﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidLake.Framework.Util
{
    public struct ImmersiveInfo
    {
        public Color Foreground;
        public Color Background;
        public Color Accent;
    }
}
