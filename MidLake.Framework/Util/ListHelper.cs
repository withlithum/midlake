﻿using System;
using System.Collections.Generic;
using MidLake.Project.Issues;

namespace MidLake.Framework.Util
{
    public static class ListHelper
    {
        public static void DeleteIssueFromList(ref List<Issue> issues, Issue issue)
        {
            if (!issues.Contains(issue)) throw new ArgumentException($"{nameof(issues)} does not contains {nameof(issue)}.", nameof(issue));
            issues.Remove(issue);
            issues.ReorderIssuesList();
        }

        public static void ReorderIssuesList(this List<Issue> issues)
        {
            foreach (var issue in issues)
            {
                issue.Number = issues.IndexOf(issue) + 1;
            }
        }
    }
}
