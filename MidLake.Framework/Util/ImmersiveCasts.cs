﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidLake.Framework.Util;
using Windows.UI.ViewManagement;

namespace MidLake.Framework.Util
{
    public static class ImmersiveCasts
    {
        public static ImmersiveInfo ToImmersiveInfo(this UISettings settings)
        {
            Contract.Requires(settings != null);
            var foreground = settings.GetColorValue(UIColorType.Foreground);
            var background = settings.GetColorValue(UIColorType.Background);
            var accent = settings.GetColorValue(UIColorType.Accent);
            return new ImmersiveInfo()
            {
                Foreground = Color.FromArgb(foreground.A, foreground.R, foreground.G, foreground.B),
                Background = Color.FromArgb(background.A, background.R, background.G, background.B),
                Accent = Color.FromArgb(accent.A, accent.R, accent.G, accent.B)
            };
        }

        public static ImmersiveInfo GetNewOne() => new UISettings().ToImmersiveInfo();

    }
}
